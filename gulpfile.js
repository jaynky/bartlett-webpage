//Modules
//sudo npm install gulp-clean-css --save-dev
//sduo npm install --save gulp-htmlmin
//sudo npm install browser-sync gulp --save-dev


var gulp        = require('gulp');
var cleanCSS = require('gulp-clean-css');
var htmlmin = require('gulp-htmlmin');
var browserSync = require('browser-sync').create();
var htmlmin = require('gulp-html-minifier');

//Minify CSS
gulp.task('minify-css', () => {
     return gulp.src('*.css')
    .pipe(cleanCSS())
    .pipe(gulp.dest('public'));
});

//Minify HTML


gulp.task('minify', function() {
  gulp.src('2Bartlett.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./public'))
      return new Promise(function(resolve, reject) {
        resolve();
});
});

//Browser sync
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});
